package com.company;

public abstract class Operation {
    public abstract void doo(int n1, int n2);
}

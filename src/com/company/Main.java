package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {
    static int n1, n2, choice;

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) throws IOException {
        HashMap<Integer,Operation> ops = new HashMap<Integer,Operation>();
        ops.put(1,new Add());
        ops.put(2,new Subtract());
        ops.put(3,new Multiply());
        ops.put(4,new Division());
        takeInput();
        takeChoice();
        Calculate(choice, n1, n2, ops);

    }

    public static void takeInput() throws IOException {
        n1 = Integer.parseInt(reader.readLine());
        n2 = Integer.parseInt(reader.readLine());
    }

    public static void takeChoice() throws IOException {
        choice = Integer.parseInt(reader.readLine());
    }

    public static void Calculate(int choice, int n1, int n2, HashMap ps){
        Operation toPerform = (Operation) ps.get(choice);
        toPerform.doo(n1, n2);
    }
}
